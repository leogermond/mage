with Ada.Real_Time; use Ada.Real_Time;
with Mage.Event;

package body Mage.Apps.Simple_Loop is
   procedure Run is
   begin
      Canvas := Draw.Get_Canvas (Window);

      Next := Clock + Period;

      Init (Canvas);

      while not Event.Is_Killed loop
         Run_Once (Canvas);
         Event.Handle_Events (Window);

         delay until Next;
         Next := Next + Period;
      end loop;
   end Run;
end Mage.Apps.Simple_Loop;
