with Ada.Real_Time;
with Mage.Draw;

generic
   Window : Draw.Window_ID;
   Period : Ada.Real_Time.Time_Span;
   with procedure Run_Once (Canvas : Draw.Canvas_ID) is <>;
   with procedure Init (Canvas : Draw.Canvas_ID) is null;
   --  Custom initialization related to canvas, before starting
   --  the loop
package Mage.Apps.Simple_Loop is
   --  Initialize the canvas, then loop on the given loop procedure,
   --  and with the given period

   Canvas : Draw.Canvas_ID;
   Next : Ada.Real_Time.Time;

   procedure Run;

end Mage.Apps.Simple_Loop;
